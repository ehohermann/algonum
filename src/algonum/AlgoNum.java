/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum;

import algonum.Lineaire.Lineaire;
import algonum.NonLineaire.NonLineaire;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author hermann
 */
public class AlgoNum {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        char rep = 'O';
        Scanner sc = new Scanner(System.in);
        int choix;
        while (rep == 'O') {
            /**
             * affichage de l'entete
             */
            System.out.println("_________________________________________________________________________________\n");
            System.out.println("\tBienvenue sur la plateforme de résolution d'equation d'agotitmique");
            System.out.println("_________________________________________________________________________________\n");

            /**
             * Boucle de controle du choix du genre
             */
            do {
                System.out.println("Choisisez le genre d'equation a utiliser");
                System.out.println("1: Equation non Linéaire");
                System.out.println("2: Equation Linéaire");
                System.out.println("3: Quiter");
                choix = sc.nextInt();
                /**
                 * affichage de la rairon de la boucle qui redemande de saisire
                 */
                if (choix != 1 && choix != 2 && choix != 3) {
                    System.out.println("Vous devez choisir un nombre entre 1 et 3");
                }
            } while (choix != 1 && choix != 2 && choix != 3);
            /**
             * switch case pour le choix du menu
             */
            switch (choix) {
                case 1:
                    NonLineaire.affichage();
                    break;
                case 2:
                    Lineaire.affichage();
                    break;
                case 3:
                    System.out.println("Au revoir");
                    break;
                default:
                    System.out.println("Cette option n'est pas intégré veuillez entrez un nombre entre 1 et 2");
                    break;
            }
            if (choix != 3) {
                /**
                 * systeme de controle de la boucle
                 */
                do {
                    System.out.print("Voulez vous continuer avec un autre type d'equation Oui/Non");
                    sc.nextLine();
                    String str = sc.nextLine();
                    str = str.toUpperCase();
                    rep = str.charAt(0);
                    if (rep != 'O' && rep != 'N') {
                        System.out.println("\nVous devez entrer Oui ou Nom");
                    }
                } while (rep != 'O' && rep != 'N');
            } else {
                rep = 'N';
            }
        }
        System.out.println("Au revoir…");
    }
}
