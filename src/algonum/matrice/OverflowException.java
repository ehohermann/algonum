/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.matrice;

/**
 *
 * @author hermann
 */
public class OverflowException extends Exception {

    public OverflowException() {
        System.out.println("Vous essayer d'aceder a un coordonné qui n'apartient pas a la matrice");
    }

    public OverflowException(String message) {
        super(message);
    }
}
