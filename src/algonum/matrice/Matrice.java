/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

rapel   Y c'est les I lingnes
        X c'est les J colones
utlisation de i0=1 et de j0=1
 */
package algonum.matrice;

import java.util.ArrayList;
import algonum.matrice.OverflowException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hermann
 */
public class Matrice {

    private ArrayList<ArrayList<Double>> A;//liste contenant tous les données de la matrice
    /**
     * nombre de Colones
     */
    private int x = 0;
    /**
     * Nombre de ligne
     */
    private int y = 0;//Nombre de ligne

    /**
     * Contructeur prenant en parametre un ArrayList déja rempli et sa dimention
     *
     * @param A
     * @param dim
     */
    public Matrice(ArrayList<ArrayList<Double>> A, int dim) {
        //this.setDim(dim);
        this.x = dim;
        this.y = dim;
        if (A == null) {
            this.initTo0();
        } else {
            this.A = A;
        }
    }

    public Matrice(double[][] table, int ligne, int colones) {
        if (this.A == null) {
            this.A = new ArrayList<>();
        }
        this.x = colones;
        this.y = ligne;
        this.initTo0();
        for (int i = 0; i < ligne; i++) {
            ArrayList<Double> line = new ArrayList<>();
            for (int j = 0; j < colones; j++) {
                try {
                    this.setIJ(i, j, table[i][j]);
                } catch (OverflowException ex) {
                    Logger.getLogger(Matrice.class.getName()).log(Level.SEVERE, null, ex);
                }
                line.add(table[i][j]);
            }
        }
    }

    public Matrice(double[] table, int ligne) {
        if (this.A == null) {
            this.A = new ArrayList<>();
        }
        for (int i = 0; i < ligne; i++) {
            ArrayList<Double> line = new ArrayList<>();
            // System.out.println("nombre de ligne " + ligne);
            line.add(table[i]);
            this.A.add(line);
        }
    }

    /**
     * constructeur par la dimention
     *
     * @param dim
     */
    public Matrice(int dim) {
        this.setDim(dim);

        this.initTo0();
    }

    /**
     *
     * @param i nombre de ligne
     * @param j Nombre de colone
     */
    public Matrice(int i, int J) {
        this.x = J;
        this.y = i;
        this.initTo0();
    }

    public ArrayList<ArrayList<Double>> getA() {
        return A;
    }

    public void setA(ArrayList<ArrayList<Double>> A) {
        this.A = A;
    }

    public ArrayList<Double> getLinge(int i) {
        return this.A.get(i);
    }

    public void setLigne(ArrayList<Double> Linge) {
        this.A.add(Linge);
    }

    public void setLigne(int indexe, ArrayList<Double> Linge) {
        this.A.add(indexe, Linge);
    }

    public double getIJ(int I, int J) throws OverflowException {
        if (this.getY() >= I && this.getY() >= 0 && I >= 0) {
            if (this.getX() >= J && this.getX() >= 0 && J >= 0) {
                ArrayList<Double> ligne = new ArrayList<>();
                ligne = this.A.get(I);
                return ligne.get(J);
            } else {
                throw new OverflowException("over flow on ne peut pas aceder a la matrice "
                        + "aux cordonnés[ " + I + ", " + J + "]; J est superieur ux dimention de la matrice");
            }
        } else {
            throw new OverflowException("over flow on ne peut pas aceder a la matrice "
                    + "aux cordonnés[ " + I + ", " + J + "]; I est superieur ux dimention de la matrice");
        }
    }

    /**
     *
     * @param I ligne I
     * @param J Colone
     * @param val
     * @throws OverflowException
     */
    public void setIJ(int I, int J, double val) throws OverflowException {
        if (this.getY() >= I) {
            if (this.getPositionY() >= I) {
                if (this.A.get(I).size() >= J) {
                    this.A.get(I).set(J, val);
                } else {

                    System.err.println("II est supereiur au nombre de colone");
                }
            } else {
                System.err.println("II est supereiur au nombre de ligne");
            }
        } else {
            System.err.println("II est supereiur au nombre de ligne");
        }
    }

    /**
     * retourne la posion de la matrice c'est a dire la deniere ligne ( au cas
     * ou la matrice n'est pas pleine) 0 pour vide
     *
     * @return
     */
    public int getPositionY() {
        if (this.A != null) {
            return this.A.size();
        } else {
            //this.A = new ArrayList<>();
            return 0;
        }
    }

    /**
     * retourne l'indexe 0 pour la colone 1
     *
     * @return
     */
    public int getIndexeOfX() {
        return this.getPositionX() - 1;
    }

    /**
     * retourene l'indexe de Y 0 pour la linge 1
     *
     * @return
     */
    public int getIndexeOfY() {
        return this.getPositionY() - 1;
    }

    public ArrayList<Double> getYValue() {
        return this.A.get(this.getIndexeOfY());
    }

    public Double getXValue() {
        return this.A.get(this.getIndexeOfY()).get(this.getIndexeOfX());
    }

    /**
     * retourne la posion de la derniere colone ( au cas ou la matrice n'est pas
     * pleine)
     *
     * @return
     */
    public int getPositionX() {
        if (this.A != null) {
            if (this.A.size() > 0) {
                int size = 0;
                if (this.getPositionY() == 0) {
                    return 0;
                }
                if (this.getYValue() != null) {
                    size = this.getYValue().size();
                } else {
                    ArrayList<Double> arr = new ArrayList<>();
                    this.A.add(arr);
                }
                return size;
            } else {
                return 0;
            }
        } else {
            //this.A = new ArrayList<>();
            return 0;
        }
    }

    /**
     * renvoi des info de dimention sur la matrice
     */
    public void toStringDim() {
        System.out.println("Dimentions de la matrice" + this.getDim());
        if (this.getDim() == 0) {
            System.out.println("Nombre de ligne " + this.getY());
            System.out.println("Nombre de colones " + this.getX());
        }
    }

    /**
     * recupere la dimention en cas de matrice carré
     *
     * @return
     */
    public int getDim() {
        if (this.x == this.y) {
            return this.x;
        } else {
            return 0;
        }
    }

    /**
     * definition de la dimention en cas de matrice carré
     *
     * @param dim
     */
    public void setDim(int dim) {
        this.x = dim;
        this.y = dim;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     * verifie si la matrice est carré
     *
     * @return true pour vrais et false pour faux
     */
    public boolean isMatriceCarre() {
        return this.x == this.y;
    }

    @Override
    public String toString() {
        if (!this.isInit()) {
            this.initTo0();
        }
        String str = new String();
        str = "[";
        for (int y1 = 0; y1 < this.y; y1++) {
            for (int x1 = 0; x1 < this.x; x1++) {
                try {
                    str = str + " " + this.getIJ(y1, x1) + " ";
                } catch (OverflowException e) {
                    e.printStackTrace();
                }
            }
            str = str + "\n";
        }
        str = str + "]";
        return str;
    }

    public void affichage() {
        System.out.println(this.toString());
    }

    public boolean isInit() {
        boolean res = true;
        if (this.A != null) {
            if (this.A.size() == this.y) {
                for (int i = 0; i < this.y; i++) {
                    if (this.A.get(i) != null) {
                        if (this.A.get(i).size() != this.x) {
                            res = false;
                        }
                    } else {
                        res = false;
                    }
                }
            } else {
                res = false;
            }
        } else {
            res = false;
        }
        return res;
    }

    /**
     * intitialisation des données a 0
     */
    public void initTo0() {
        this.A = new ArrayList<>();
        for (int i = 0; i < this.y; i++) {
            ArrayList<Double> ligne = new ArrayList<>();
            for (int j = 0; j < this.x; j++) {
                //System.out.println("[" + i + " " + j + " ]");
                ligne.add(0.0);
            }
            this.A.add(ligne);
        }
    }

    public Matrice getL() {
        Matrice L = null;
        if (this.isMatriceCarre()) {
            int dim = this.getDim();
            L = new Matrice(dim);
            try {
                for (int l = 0; l < dim; l++) {
                    //traitement de la tiangulaire inférieur
                    for (int c = 0; c < l; c++) {
                        L.setIJ(l, c, this.getIJ(l, c));
                    }
                    //traitemet de la triangulaire supérieur
                    for (int c = l; c < dim; c++) {
                        L.setIJ(l, c, 0);
                    }
                    //traitement de la diagonale
                    L.setIJ(l, l, 1);
                }
            } catch (OverflowException e) {
                e.printStackTrace();
            }
        }
        return L;
    }

    public Matrice getR() {
        Matrice R = null;
        if (this.isMatriceCarre()) {
            int dim = this.getDim();
            R = new Matrice(dim);
            try {
                for (int l = 0; l < dim; l++) {
                    //traitement de la diagoanle supérieur
                    for (int c = l; c < dim; c++) {
                        R.setIJ(l, c, this.getIJ(l, c));
                    }
                    //traitement de la diagonale inférieur
                    for (int c = 0; c < l; c++) {
                        R.setIJ(l, c, 0);
                    }
                }
            } catch (OverflowException e) {
                e.printStackTrace();
            }
        }
        return R;
    }

    public void execLR() {
        int n = this.getDim();
        Matrice L = this.getL();
        Matrice R = this.getR();
        try {
            for (int i = 0; i < n; i++) {
                R.setIJ(1, i, this.getIJ(1, i));
                L.setIJ(i, 1, this.getIJ(i, 1) / this.getIJ(1, 1));
            }
            for (int i = 1; i < n; i++) {
                for (int j = i; j < n; j++) {
                    double somme = 0;
                    for (int k = 0; k < i - 1; k++) {
                        somme = somme + L.getIJ(i, k) * R.getIJ(k, j);
                    }
                    R.setIJ(i, j, this.getIJ(i, j) - somme);
                }
                for (int k = i + 1; k < n && i < n - 1; k++) {
                    double somme = 0;
                    for (int j = 0; j < i - 1; j++) {
                        somme = somme + L.getIJ(k, j) * R.getIJ(j, i);
                    }
                    L.setIJ(k, i, (this.getIJ(k, i) - somme) * R.getIJ(i, i));
                }
            }
        } catch (Exception e) {
        }
    }

    public Matrice getLt() {
        return new Matrice(x);
    }
}
