/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.exception;

/**
 *
 * @author hermann
 */
public class EqualBorneException extends Exception {

    public EqualBorneException() {
        System.out.println("Vous essayer d'aceder a un coordonné qui n'apartient pas a la matrice");
    }

    public EqualBorneException(String message) {
        super(message);
    }
}
