/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.exception;

/**
 *
 * @author hermann
 */
public class DerivateNullException extends Exception {

    public DerivateNullException() {
        System.out.println("La dérivée est null");
    }

    public DerivateNullException(String message) {
        super(message);
    }
}
