/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.exception;

/**
 *
 * @author hermann
 */
public class DenumNullException extends Exception {

    public DenumNullException() {
        System.out.println("Le denominateur est null");
    }

    public DenumNullException(String message) {
        super(message);
    }
}
