/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire;

import java.util.ArrayList;
import algonum.NonLineaire.Intervale;

/**
 *
 * @author hermann
 */
public abstract class Balayage extends Fonction {

    public static ArrayList exec() {
//        Scanner sc = new Scanner(System.in);
        System.out.println("le systeme avec la fonction fx=0 avec pour ampliturde 0.001");
//        
        double amplitude = 0.001;
        ArrayList<Intervale> listeIntervale = new ArrayList<Intervale>();
        Intervale intervale = new Intervale();
        /**
         * ON VERIFIE SI L'UNE DES BORNE EST SOLUTION DE L'EQUATION
         *
         */
        if (Balayage.Fx(intervale.getInf()) * Balayage.Fx(intervale.getSup()) == 0) {
            if (Balayage.Fx(intervale.getInf()) == 0 && Balayage.Fx(intervale.getSup()) == 0) {
                System.out.println(intervale.getInf() + " et " + intervale.getSup()
                        + " sont tous deux solutions de cette equation\n");
            } else if (Balayage.Fx(intervale.getInf()) == 0 && Balayage.Fx(intervale.getSup()) != 0) {
                System.out.println(intervale.getInf() + " est une des solutions de l'equation\n");
            } else if (Balayage.Fx(intervale.getInf()) != 0 && Balayage.Fx(intervale.getSup()) == 0) {
                System.out.println((double) (intervale.getSup()) + "est une des solutions de l'equation\n");
            }
        } else {
            //nbrSol = 0;
            for (double i = intervale.getInf(); i < intervale.getSup(); i += amplitude) {
                /**
                 * si on a une solution
                 */
                try {
                    Intervale interv = new Intervale(i, i + amplitude);
                    if (Balayage.isIntSol(interv)) // if (Balayage.Fx(interv.getInf()) * Balayage.Fx(interv.getSup()) == 0) 
                    {
                        listeIntervale.add(interv);
                    }
                } catch (Exception e) {
                }
            }
            if (listeIntervale.size() >= 1) {
                System.out.println("Nous avons " + listeIntervale.size()
                        + " interval solution dans l'inetrvale saise [ " + intervale.getInf()
                        + ", " + intervale.getSup() + "]");
                for (int i = 0; i < listeIntervale.size(); i++) {
                    System.out.println(listeIntervale.get(i));
                    Balayage.solInt(listeIntervale.get(i));
                }
            }
            if (listeIntervale.size() < 1) {
                System.out.println("il n'y a pas de solution dans cet Intervale");
            }
        }
        return listeIntervale;

    }

    public static boolean isIntSol(Intervale intervale) {
        boolean res = false;
        if (Balayage.Fx(intervale.getInf()) * Balayage.Fx(intervale.getSup()) <= 0) {
            res = true;
        }
        return res;
    }

    public static void solInt(Intervale intervale) {
        if (Balayage.isIntSol(intervale)) {
            if (Balayage.Fx(intervale.getInf()) == 0 && Balayage.Fx(intervale.getSup()) == 0) {
                System.out.println(intervale.getInf() + " et " + intervale.getSup() + " Sont tous les deux solution de l'équation");
            }
            if (Balayage.Fx(intervale.getInf()) != 0 && Balayage.Fx(intervale.getSup()) == 0) {
                System.out.println(intervale.getSup() + " est une solution de l'équation");
            }
            if (Balayage.Fx(intervale.getInf()) == 0 && Balayage.Fx(intervale.getSup()) != 0) {
                System.out.println(intervale.getInf() + " est une solution de l'équation");
            }
        }
    }
}
