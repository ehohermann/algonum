/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire;

import java.util.Scanner;

/**
 *
 * @author hermann
 */
public abstract class NonLineaire {

    public static void affichage() {

        char rep = 'O';
        Scanner sc = new Scanner(System.in);
        int choix;
        System.out.println(Math.atan(0));
        System.out.println("arctang de 01");
        while (rep == 'O') {
            /**
             * affichage de l'entete
             */
            System.out.println("___________________________________________________________________________________________________\n");
            System.out.println("\tBienvenue sur la plateforme de résolution d'equation Non linéaire\n");
            System.out.println("___________________________________________________________________________________________________\n\n");

            /**
             * Boucle de controle du choix du genre
             */
            do {
                System.out.println("Choisisez la methode à utiliser");
                System.out.println("1: Methode de balayage / corollaire du théorème de Rolle OK");
                System.out.println("2: Methode de substitution OK");
                System.out.println("3: Methode de Dichotomie ou de la bisection OK");
                System.out.println("4: Methode de Newton-Raphson");
                System.out.println("5: Methode de la sécante ou de la corde");
                System.out.println("6: Methode d'interpolation linéaire");
                choix = sc.nextInt();
            } while (choix < 1 || choix > 6);

            switch (choix) {
                case 1:
                    Balayage.exec();
                    break;
                case 2:
                    Substitution.exec();
                    break;
                case 3:
                    Dichotomie.exec();
                    break;
                case 4:
                    //Newton Raphson
                    NewtonRaphson.exec();
                    break;
                case 5:
                    //Cord
                    Fonction.execCorde();
                case 6:
                    //interpolation
                    Fonction.execInterpolation();
                    break;
                default:
                    System.out.println("Cette option n'est pas intégré veuillez entrez un nombre entre 1 et 2");
                    break;
            }
            /**
             * systeme de controle de la boucle
             */
            do {
                System.out.println("Voulez vous executer une nouvel equation non lineaire? Oui/Non");
                String str = sc.nextLine();
                str = sc.nextLine();
                str = str.toUpperCase();
                rep = str.charAt(0);
                if (rep != 'O' && rep != 'N') {
                    System.out.println("Vous devez entrer Oui ou Nom");
                }
            } while (rep != 'O' && rep != 'N');

        }
        System.out.println("Au revoir…");
    }

    public static void execBalayage() {
        Balayage.exec();
    }

    public static void execDichotomie() {
    }

    public static void execNR() {

    }
}
