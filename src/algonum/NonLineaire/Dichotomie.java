/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire;

import java.util.Scanner;

/**
 *
 * @author EHO hermann
 * @description La méthode de dichotomie ou méthode de la bissection
 */
public abstract class Dichotomie extends Fonction {

    public static void exec() {
        Scanner sc = new Scanner(System.in);
        System.out.println("le systeme avec la fonction fx=x^2 -1 avec pour "
                + "ampliturde 0.001 et pour point de depart 2");

        double a, b, m;
        double amplitude = 0.001;
        //System.out.println("entrez la valeur de a");
        a = -3.15 / 2;
        //System.out.println("entrez la valeur de b");
        b = 0;
        System.out.println("intervale [ " + a + "," + b + "]");
        
        /**
         * Si f(a) et f(b) on un signe différent. Donc il existe au moins un Zero
         * Dans le cas contraire on ne sais pas s'il y a un zero ou pas. 
         */
        if (Dichotomie.Fx(a) * Dichotomie.Fx(b) <= 0) {
            
        }
        
        /**
         * Application de la fonction pour déterminer les bornes
         * Fonction de calcul de dichotomie
         */
        do {
            m = (a + b) / 2;
            if (Dichotomie.Fx(a) * Dichotomie.Fx(m) <= 0) {
                b = m;
            } else {
                a = m;
            }
        } while ((b - a) > amplitude);

        if (Dichotomie.Fx(a) * Dichotomie.Fx(b) == 0) {
            if (Dichotomie.Fx(a) == 0 && Dichotomie.Fx(b) == 0) {
                System.out.println("A " + a + " et B " + b + " Sont tous les "
                        + "deux solution de l'equation");
            }
            if (Dichotomie.Fx(a) == 0 && Dichotomie.Fx(b) != 0) {
                System.out.println("A " + a + " est la solution de l'equation");
            }
            if (Dichotomie.Fx(a) != 0 && Dichotomie.Fx(b) == 0) {
                System.out.println("B " + b + " est la solution de l'equation");
            }
        } else if (Dichotomie.Fx(a) * Dichotomie.Fx(b) >amplitude) {
            System.out.println("il n'y a pas de solution dans l'intervale");
        } else {
            System.out.println((b + a) / 2 + " est la solution de l'equation");
        }
    }
}
