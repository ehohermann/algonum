/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire;

import algonum.exception.DenumNullException;
import algonum.exception.DerivateNullException;

/**
 * @author hermann
 */
public abstract class Fonction {

    /**
     * @var er Erreur relative er >= ||xn - xn-1||
     * @var ea Erreur absolut ea >= ||xn - xn-1|| / ||Xn||
     */
    protected static final double er = 0.0001, ea = 0.00001;

    protected static double Fx(double x) {
        //return 2 * Math.tan(x) - 1;
        //return Math.pow(x, 2) - 1;
        //return Math.pow(x, 3) + 3 * x * x + 2 * x;
        //return Math.atan((x + 1) / 2);
        return Math.pow(x, 2) + 1;
    }

    protected static double Gx(double x) {
        //return Math.atan((x + 1) / 2);
        //return 0.0;
        return x - Fonction.Fx(x);
    }

    protected static double derivative(double x) {
        return (0.5) * (4 / (4 + Math.pow((x + 1), 2)));
    }

    //Newton Raphson Funtion
    protected static double NRFunction(double x) throws DerivateNullException {
        if (Fonction.derivative(x) == 0) {
            throw new DerivateNullException();
        }
        return x - (Fonction.Gx(x) / Fonction.derivative(x));
    }

    //Newton Raphson Funtion
    /**
     *
     * @param x
     * @param xn1 valeur xn-1
     * @return la valeur de xn+1
     * @throws DenumNullException
     */
    protected static double interpolationFunction(double x, double xn1) throws DenumNullException {
        double num = Fonction.Fx(x) * xn1 - Fonction.Fx(xn1) * x;
        double denum = Fonction.Fx(x) - Fonction.Fx(xn1);
        if (denum == 0) {
            throw new DenumNullException();
        }
        return num / denum;
    }

    /**
     *
     * @param x
     * @param xn1
     * @return
     * @throws DenumNullException
     */
    protected static double cordeFunction(double x, double xn1) throws DenumNullException {
        double num = Fonction.Gx(x) * xn1 - Fonction.Gx(xn1) * x;
        double denum = Fonction.Gx(x) - Fonction.Gx(xn1);
        if (denum == 0) {
            throw new DenumNullException();
        }
        return num / denum;
    }

    public static void execCorde() {
        double x0 = 100;
        double xn, xn1, x1n = 0;
        int i = 0;
        double dif = 1;
        boolean broken = false;
        for (xn = x0; er < dif; i++) {
            try {
                xn1 = Fonction.cordeFunction(xn, x1n);
            } catch (DenumNullException ex) {
                broken = true;
                System.out.println("Le denominateur est null");
                break;
            }
            dif = Math.abs(xn - xn1);
            x1n = xn;
            xn = xn1;
            if (i > 100) {
                broken = true;
                System.out.println("l'itération est trop long Prenez un point X0 "
                        + "plus proche de la solution la boucle serais arreter ");
                break;
            }
        }
        if (broken == false) {
            System.out.println("la Solution de l'equation x^2 +1 est " + xn);
            System.out.println("apres " + i + " itérations");
        } else {
            System.out.println("La bouble a été arreter");
        }

    }

    public static void execInterpolation() {
        double x0 = 100;
        double xn, xn1, x1n = 0;
        int i = 0;
        double dif = 1;
        boolean broken = false;
        for (xn = x0; er < dif; i++) {
            try {
                xn1 = Fonction.cordeFunction(xn, x1n);
            } catch (DenumNullException ex) {
                broken = true;
                System.out.println("Le denominateur est null");
                break;
            }
            dif = Math.abs(xn - xn1);
            x1n = xn;
            xn = xn1;
            if (i > 100) {
                broken = true;
                System.out.println("l'itération est trop long Prenez un point X0 "
                        + "plus proche de la solution la boucle serais arreter ");
                break;
            }
        }
        if (broken == false) {
            System.out.println("la Solution de l'equation x^2 +1 est " + xn);
            System.out.println("apres " + i + " itérations");
        } else {
            System.out.println("La bouble a été arreter");
        }
    }

    @Override
    public String toString() {
        return "Fonction{ x^2 +1}";
    }

}
