/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire.methodes;

import algonum.NonLineaire.utilities.Fonction;
import algonum.NonLineaire.utilities.Methode;
import algonum.exception.DenumNullException;

/**
 *
 * @author hermann
 */
public class Corde extends Methode {

    private int iteration;
    private double x0, x1;

    public Corde(double x0, double x1) {
        this.setX0(x0);
        this.setX1(x1);

        //double xn, xn1 = 0;
        double x2;
        int i = 0;
        double dif = 1;
        boolean broken = false;
        //loop while the relative error is acceptable
        for (; Fonction.er < dif; i++) {
            try {
                x2 = Fonction.getSuiteFonctionApproche(x0, x1);
            } catch (DenumNullException ex) {
                broken = true;
                System.out.println("Erreur : Le dénominateur est null");
                break;
            }
            //finding the précission of the result
            dif = Math.abs(x2 - x1);

            x0 = x1;
            x1 = x2;
            if (i > 10000) {
                broken = true;
                System.out.println("Warning: l'itération est trop long Prenez un point X0 "
                        + "plus proche de la solution la boucle serais arreter ");
                break;
            }
        }
        if (broken == false) {
            this.setSolution(x1);
            this.setIteration(i);
        } else {
            System.out.println("La fonction s'est arreter suite a une erreur");
        }
    }

    //Getter and setters
    public int getIteration() {
        return iteration;
    }

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    public double getX0() {
        return x0;
    }

    public void setX0(double x0) {
        this.x0 = x0;
    }

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    @Override
    public void printResult() {
        System.out.println("la Solution de l'equation x^2 +1 est " + this.getSolution());
        System.out.println("apres " + this.getIteration() + " itérations");
    }
}
