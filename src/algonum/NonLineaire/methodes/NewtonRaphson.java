/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire.methodes;

import algonum.NonLineaire.utilities.Fonction;
import algonum.NonLineaire.utilities.Methode;
import algonum.exception.DerivateNullException;

/**
 *
 * @author hermann
 */
public class NewtonRaphson extends Methode {

    private int iteration;

    public NewtonRaphson(double x0) {
        double xn, xn1 = 0;
        int i = 0;
        double dif = 1;
        boolean broken = false;
        //loop while the relative error is acceptable
        for (xn = x0; Fonction.er < dif; i++) {
            try {
                xn1 = this.getIteration(xn);
            } catch (DerivateNullException ex) {
                broken = true;
                System.out.println("Erreur : La dérivé est null");
                break;
            }
            //finding the précission of the result
            dif = Math.abs(xn - xn1);
            xn = xn1;
            if (i > 10000) {
                broken = true;
                System.out.println("Warning: l'itération est trop long Prenez un point X0 "
                        + "plus proche de la solution la boucle serais arreter ");
                break;
            }
        }
        if (broken == false) {
            this.setSolution(xn);
            this.setIteration(i);
        } else {
            System.out.println("La fonction s'est arreter suite a une erreur");
        }
    }

    /**
     *
     * @param x variable to calculate
     * @return the iteration
     */
    public double getIteration(double x) throws DerivateNullException {
        if (Fonction.getDerivative(x) == 0) {
            throw new DerivateNullException();
        }
        return x - (Fonction.getResult(x) / Fonction.getDerivative(x));
    }
    
    //Getter and setters
    public int getIteration() {
        return iteration;
    }

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    @Override
    public void printResult() {
        System.out.println("la Solution de l'equation x^2 +1 est " + this.getSolution());
        System.out.println("apres " + this.getIteration() + " itérations");
    }
}
