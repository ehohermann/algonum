/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire;

import algonum.exception.DerivateNullException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hermann
 */
public abstract class NewtonRaphson extends Fonction {

    public static void exec() {
        double x0 = 0.706;
        double xn, xn1 = 0;
        int i = 0;
        double dif = 1;
        boolean broken = false;
        for (xn = x0; er < dif; i++) {
            try {
                xn1 = NewtonRaphson.NRFunction(xn);
            } catch (DerivateNullException ex) {
                broken = true;
                System.out.println("La dérivé est null");
                break;
            }
            dif = Math.abs(xn - xn1);
            xn = xn1;
            if (i > 10000) {
                broken = true;
                System.out.println("l'itération est trop long Prenez un point X0 "
                        + "plus proche de la solution la boucle serais arreter ");
                break;
            }
        }
        if (broken == false) {
            System.out.println("la Solution de l'equation x^2 +1 est " + xn);
            System.out.println("apres " + i + " itérations");
        } else {
            System.out.println("La bouble a été arreter");
        }
    }
}
