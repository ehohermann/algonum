/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire;

import java.util.Scanner;

/**
 *
 * @author hermann
 */
public abstract class Substitution extends Fonction {

    public static void exec() {

        //System.out.println("le systeme avec la fonction fx=x^2 - x +1 avec pour ampliturde 0.001 et pour point de depart 2");
        double x1 = -3.14/2;
        double xn = 0;
        double xnm1 = 0;
        double amplitude = 0.00001;
        xn = x1;
        int i = 0;
        do {
            xnm1 = xn;
            xn = Substitution.Gx(xn);
            System.out.println(xn);
            i++;
        } while (Math.abs(xn - xnm1) > amplitude && i < 100000);
        if (i > 99) {
            System.out.println("erreur de convergence");
        }
        if (Substitution.Gx(xn) == xn) {
            System.out.println("la solution de l'equation est " + xn);
        } else {
            //System.out.println("la solution existe est " + xn);
            System.out.println("fx(" + xn + ")=" + Substitution.Fx(xn));
        }
    }

}
