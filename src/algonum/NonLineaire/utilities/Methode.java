/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire.utilities;

import algonum.NonLineaire.Intervale;
import java.util.ArrayList;

/**
 *
 * @author hermann
 */
public class Methode {

    protected ArrayList results;
    protected Intervale intervale;
    protected ArrayList<Intervale> intervales;
    protected double solution;
    protected ArrayList<Double> solutions;

    //Getter and setters
    //Setters
    public void setResults(ArrayList results) {
        this.results = results;
    }

    public void setIntervale(Intervale intervale) {
        this.intervale = intervale;
    }

    public void setIntervales(ArrayList<Intervale> intervales) {
        this.intervales = intervales;
    }

    public void setSolution(double solution) {
        this.solution = solution;
    }

    public void setSolutions(ArrayList<Double> solutions) {
        this.solutions = solutions;
    }

    //Getters
    //Get the respons if there is one or more intervale
    public Intervale getIntervale() {
        try {
            return new Intervale(0, 1);
        } catch (Exception e) {
        }
        //if the try cach failed Create new intervale trought Stdin
        return null;
    }

    //get all the response if there is many intervales 
    public ArrayList<Intervale> getIntervales() {
        return new ArrayList<Intervale>();
    }

    //Get the result if there is only one result
    public double getSolution() {
        return 0;
    }

    //Get the result if there is more result
    public ArrayList<Double> getSolutions() {
        return new ArrayList<Double>();
    }

    public ArrayList getResults() {
        return new ArrayList<>();
    }

    public void printResult() {

    }

}
