/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.NonLineaire.utilities;

import algonum.exception.DenumNullException;

/**
 *
 * @author hermann
 */
public abstract class Fonction {

    /**
     * @var er Erreur relative er >= ||xn - xn-1||
     * @var ea Erreur absolut ea >= ||xn - xn-1|| / ||Xn||
     */
    public static final double er = 0.0001, ea = 0.00001;

    /**
     * Renvoi le résultat de la fonction
     * @param x x est la variable de la fonction
     * @return retusltat de la fonction
     */
    public static double getResult(double x) {
        return Math.pow(x, 2) + 1;
    }

    public static String getName() {
        return "Nom de la Fonction X carré +1 ";
    }

    public static double getDerivative(double x) {
        return 0;
    }

    /**
     * L'expression raproché est une dérivé de la fonction
     *
     * @param x0
     * @param x1
     * @return
     * @throws DenumNullException
     */
    public static double getFonctionApproche(double x0, double x1) throws DenumNullException {
        double num = Fonction.getResult(x1) - Fonction.getResult(x0);
        double denum = x1 - x0;
        if (denum == 0) {
            throw new DenumNullException();
        }
        return num / denum;
    }

    /**
     *
     * @param x0
     * @param x1
     * @return x2
     * @throws DenumNullException
     */
    public static double getSuiteFonctionApproche(double x0, double x1) throws DenumNullException {
        double num = Fonction.getResult(x1) * x0 - Fonction.getResult(x0) * x1;
        double denum = Fonction.getResult(x1) - Fonction.getResult(x0);
        if (denum == 0) {
            throw new DenumNullException();
        }
        return num / denum;
    }
}
