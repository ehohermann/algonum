/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.Lineaire;

import algonum.matrice.Matrice;
import algonum.matrice.OverflowException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hermann
 */
public class GaussJordan {

    /**
     * @var matrice principale Carré
     */
    Matrice A;
    /**
     * @var Matrice B (vecteur B)
     */
    Matrice B;
    /**
     * @var Matrice Augmenté
     */
    Matrice generale;
    /**
     * les resultats de la matrice
     */
    ArrayList<Matrice> res = new ArrayList<Matrice>();

    public GaussJordan() {
    }

    public GaussJordan(Matrice A, Matrice B) {
        if (A.isMatriceCarre()) {
            this.A = A;
        } else {
            System.out.println("la matrice n'est pas carré");
        }
        this.B = B;
    }

    public Matrice getA() {
        return A;
    }

    public void setA(Matrice A) {
        this.A = A;
    }

    public Matrice getB() {
        return B;
    }

    public void setB(Matrice B) {
        this.B = B;
    }

    public Matrice getGenerale() {
        return generale;
    }

    public void setGenerale(Matrice generale) {
        this.generale = generale;
    }

    public ArrayList<Matrice> getRes() {
        return res;
    }

    public void setRes(ArrayList<Matrice> res) {
        this.res = res;
    }

    /**
     * genere la matrice augmenté en fusionnant A et B
     */
    public void matGene() {
        if (this.A.isMatriceCarre()) {
            Matrice matrice = new Matrice(A.getDim(), A.getDim() + B.getX());
            for (int i = 0; i < A.getDim(); i++) {
                for (int j = 0; j < A.getDim(); j++) {
                    try {
                        matrice.setIJ(i, j, this.A.getIJ(i, j));
                    } catch (OverflowException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    matrice.setIJ(i, A.getDim(), this.B.getIJ(i, 0));
                } catch (OverflowException e) {
                    e.printStackTrace();
                }
            }
            this.generale = matrice;
        }
    }

    /**
     * test d'initialisation
     */
    public void intText() {
        this.A.affichage();
        this.B.affichage();
        System.out.println("test d'initialisation et d'affichage d'une matrice carré");
        Matrice C = new Matrice(4);
        C.affichage();
        System.out.println("test d'initialisation et d'affichage d'une matrice rectangulaire");
        Matrice D = new Matrice(4, 6);
        D.affichage();
        D = new Matrice(4, 2);
        D.affichage();
    }

    public void run() {
        System.out.println("Methode de Gauss Jordan");
        this.matGene();
        System.out.println("Matrice A");
        this.A.affichage();
        System.out.println("Matrice B");
        this.B.affichage();
        System.out.println("Matrice Augmenté");
        this.generale.affichage();
        int n = this.A.getX();
        this.res.add(this.generale);
        System.out.println(n);
        Matrice resNew = this.generale;

        for (int b = 0; b < n; b++) {
            Matrice matCourant = res.get(b);
            resNew = this.res.get(b);
            for (int a = 0; a < n; a++) {
                if (a != b) {
                    try {
                        double z = matCourant.getIJ(a, b) / matCourant.getIJ(b, b);
                        for (int c = 0; c < n + 1; c++) {
                            resNew.setIJ(a, c, (matCourant.getIJ(a, c) - z * matCourant.getIJ(b, c)));
                        }
                    } catch (OverflowException ex) {
                        Logger.getLogger(GaussJordan.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            this.res.add(resNew);
        }
//        for (int j = 1; j <= n; j++) {
//            Matrice matCourant = res.get(j - 1);
//            if (this.res.size() > 0) {
//                resNew = this.res.get(j - 1);
//            } else {
//                matCourant = this.generale;
//            }
//            for (int i = j + 1; i <= n + 1; i++) {
//                try {
//                    resNew.setIJ(j - 1, i - 1, matCourant.getIJ(j - 1, i - 1) / matCourant.getIJ(j - 1, j - 1));
//                    for (int k = 1; k <= n && k != j; k++) {
//                        resNew.setIJ(k - 1, i - 1, (matCourant.getIJ(k - 1, i - 1) - (matCourant.getIJ(j - 1, i - 1) * matCourant.getIJ(k - 1, j - 1))));
//                    }
//                } catch (OverflowException e) {
//                }
//            }
//            this.res.add(resNew);
//        }
        for (int i = 0; i < this.res.size(); i++) {
            System.out.println("Pour k = " + i);
            this.res.get(i).affichage();
        }
        System.out.println("Solution");
        System.out.print("[");
        for (int i = 0; i < this.A.getDim(); i++) {
            try {
                System.out.println(this.res.get(this.A.getDim() - 1).getIJ(i, this.A.getDim()));
            } catch (OverflowException e) {
            }
        }
        System.out.print("]");
    }

    public void runNew() {
        System.out.println("Methode de Gauss Jordan");
        this.matGene();
        System.out.println("Matrice A");
        this.A.affichage();
        System.out.println("Matrice B");
        this.B.affichage();
        System.out.println("Matrice Augmenté");
        this.generale.affichage();
        int n = this.A.getX();
        this.res.add(this.generale);
        System.out.println(n);
        Matrice resNew = this.generale;

        Matrice matCourant = this.generale;

        try {
            for (int b = 0; b < n; b++) {
                //definit les valeur en bas du pivo null
                for (int a = 0; a < b; a++) {
                    matCourant.setIJ(b, a, 0);
                }
                for (int a = b + 1; a < n + 1; a++) {
                    double z = matCourant.getIJ(b, a) / matCourant.getIJ(b, b);
                    matCourant.setIJ(b, a, z);
                    for (int c = 0; c < n; c++) {
                        if (c != b) {
                            matCourant.setIJ(c, a, (matCourant.getIJ(c, a) - z * matCourant.getIJ(c, b)));
                        }
                    }
                }
                //division du pivot pour trouver 1
                matCourant.setIJ(b, b, matCourant.getIJ(b, b) / matCourant.getIJ(b, b));
                
                System.out.println("division par le pivot");
                matCourant.affichage();
                this.res.add(matCourant);
            }
        } catch (OverflowException ex) {
            Logger.getLogger(GaussJordan.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Solution");
        System.out.print("[");
        for (int i = 0; i < this.A.getDim(); i++) {
            try {
                System.out.println(this.res.get(this.A.getDim() - 1).getIJ(i, this.A.getDim()));
            } catch (OverflowException e) {
            }
        }
        System.out.print("]");
    }
}
