/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.Lineaire;

import algonum.Lineaire.Gauss.*;
import algonum.matrice.Matrice;
import java.util.Scanner;

/**
 *
 * @author hermann
 */
public abstract class Lineaire {

    public static void affichage() {

        char rep = 'O';
        Scanner sc = new Scanner(System.in);
        int choix;
        while (rep == 'O') {
            /**
             * affichage de l'entete
             */
            System.out.println("_____________________________________________________________________________\n");
            System.out.println("\tBienvenue sur la plateforme de résolution d'equation linéaire");
            System.out.println("_____________________________________________________________________________\n");

            /**
             * Boucle de controle du choix du genre
             */
            do {
                System.out.println("Choisisez la methode à utiliser");
                System.out.println("1: Methode de gauss sans pivot");
                System.out.println("2: Methode de gauss paritel ");
                System.out.println("3: Methode de gauss total");
                System.out.println("4: Methode de Gauss-Jordan");
                System.out.println("5: Methode de jacobi");
                System.out.println("6: Quiter");

                choix = sc.nextInt();
            } while (choix < 1 || choix > 6);

            if (choix != 6) {
                switch (choix) {
                    case 1:
                        Lineaire.execGaussSansPivot();
                        break;
                    case 2:
                        Lineaire.execGaussWithPartialPivot();
                        break;
                    case 3:
                        Lineaire.execGaussWithTotalPivot();
                        break;
                    case 4:
                        Lineaire.execGaussJordan();
                        break;
                    case 5:
                        Lineaire.execJacobi();
                        break;
                    default:
                        System.out.println("Cette option n'est pas intégré  veuillez entrez un nombre entre 1 et 2");
                        break;
                }
                /**
                 * systeme de controle de la boucle
                 */
                do {
                    System.out.println("Voulez vous executer une nouvel equation lineaire? Oui/Non");
                    String str = sc.nextLine();
                    str = sc.nextLine();
                    str = str.toUpperCase();
                    rep = str.charAt(0);
                    if (rep != 'O' && rep != 'N') {
                        System.out.println("Vous devez entrer Oui ou Nom");
                    }
                } while (rep != 'O' && rep != 'N');
            } else {
                rep = 'N';
            }

        }
        System.out.println("Au revoir…");
    }

    public static void execSubs() {

    }

    public static Matrice getMatriceA() {
        double table[][] = {{1, 1, 1}, {2, 2, 5}, {4, 6, 8}};
        Matrice A = new Matrice(table, 3, 3);
        return A;
    }

    public static Matrice getMatriceB() {
        double tableB[][] = {{0}, {-3}, {4}};
        Matrice B = new Matrice(tableB, 3, 1);
        return B;
    }

    public static Matrice getMatriceInit() {
        double tableB[][] = {{1}, {-1}, {2}};
        Matrice B = new Matrice(tableB, 3, 1);
        return B;
    }

    public static void execGaussSansPivot() {
        Gauss gs = new Gauss(Lineaire.getMatriceA(), Lineaire.getMatriceB());
        if (gs.verif()) {
            gs.runSansPivot();
        }
    }

    public static void execGaussJordan() {
        GaussJordan gsj = new GaussJordan(Lineaire.getMatriceA(), Lineaire.getMatriceB());
        gsj.runNew();
    }

    public static void execLR() {
        GaussJordan gsj = new GaussJordan(Lineaire.getMatriceA(), Lineaire.getMatriceB());
        gsj.runNew();
    }

    public static void execGaussWithPartialPivot() {
        GaussPatial gs = new GaussPatial(Lineaire.getMatriceA(), Lineaire.getMatriceB());
        gs.runWithPartialPivot();
    }

    public static void execGaussWithTotalPivot() {
    }

    public static void execJacobi() {
        Jacobi gs = new Jacobi(Lineaire.getMatriceA(), Lineaire.getMatriceB());
        gs.run(Lineaire.getMatriceInit());
    }
}
