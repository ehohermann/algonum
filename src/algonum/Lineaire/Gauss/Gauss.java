/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.Lineaire.Gauss;

import algonum.matrice.Matrice;
import algonum.matrice.OverflowException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hermann
 */
public class Gauss {

    Matrice A;
    Matrice B;
    Matrice generale;
    /**
     * les resultats de la matrice
     */
    ArrayList<Matrice> res = new ArrayList<Matrice>();

    public Gauss(Matrice A, Matrice B) {
        if (A.isMatriceCarre()) {
            this.A = A;
        } else {
            System.out.println("la matrice n'est pas carré");
        }
        this.B = B;
    }

    public Matrice getA() {
        return A;
    }

    public void setA(Matrice A) {
        this.A = A;
    }

    public Matrice getB() {
        return B;
    }

    public void setB(Matrice B) {
        this.B = B;
    }

    /**
     * recupre tous les resultats
     *
     * @return
     */
    public ArrayList<Matrice> getRes() {
        return res;
    }

    /**
     * defini un resultat
     *
     * @param res
     */
    public void setRes(ArrayList<Matrice> res) {
        this.res = res;
    }

    /**
     * test d'initialisation
     */
    public void intText() {
        this.A.affichage();
        this.B.affichage();
        System.out.println("test d'initialisation et d'affichage d'une matrice carré");
        Matrice C = new Matrice(4);
        C.affichage();
        System.out.println("test d'initialisation et d'affichage d'une matrice rectangulaire");
        Matrice D = new Matrice(4, 6);
        D.affichage();
        D = new Matrice(4, 2);
        D.affichage();
    }

    public boolean verif() {
        for (int i = 0; i < this.getA().getX(); i++) {
            try {
                if (this.A.getIJ(i, i) == 0) {
                    System.out.println("Matrice Synguliere");
                    return false;
                }
            } catch (Exception e) {
            }
        }
        return true;
    }

    /**
     * raitement de la methode de gausse avec une matrice definie
     */
    public void runSansPivot() {
        /**
         * Affichage
         */
        System.out.println("Methode de Gauss sans pivot");
        this.matGene();
        System.out.println("Matrice A");
        this.A.affichage();
        System.out.println("Matrice B");
        this.B.affichage();
        System.out.println("Matrice Augmenté");
        this.generale.affichage();
        
        int n = this.A.getX();
        this.res.add(this.generale);
        System.out.println(n);
        for (int k = 1; k <= n - 1; k++) {
            Matrice resNew = new Matrice(this.generale.getY(), this.generale.getX());
            for (int i = 1; i <= k; i++) {
                for (int j = 1; j <= n + 1; j++) {
                    try {
                        resNew.setIJ(i - 1, j - 1, res.get(k - 1).getIJ((i - 1), (j - 1)));
                    } catch (OverflowException e) {
                    }
                }
            }
            try {
                /**
                 * verification des pivot
                 */
                if (res.get(k - 1).getIJ(k - 1, k - 1) != 0) {
                    for (int i = k + 1; i <= n; i++) {
                        for (int j = 1; j <= k; j++) {
                            try {
                                resNew.setIJ(i - 1, j - 1, 0.0);
                            } catch (OverflowException e) {
                            }
                        }
                    }
                    for (int i = k + 1; i <= n; i++) {
                        for (int j = k + 1; j <= n + 1; j++) {
                            try {
                                double Aijk = res.get(k - 1).getIJ(i - 1, j - 1);
                                double Aikk = res.get(k - 1).getIJ(i - 1, k - 1);
                                double Akkk = res.get(k - 1).getIJ(k - 1, k - 1);
                                double Akjk = res.get(k - 1).getIJ(k - 1, j - 1);
                                double val = Aijk - (Aikk / Akkk * Akjk);
                                resNew.setIJ(i - 1, j - 1, val);
                            } catch (OverflowException e) {
                            }
                        }
                    }

                    this.res.add(resNew);
                } else {
                    System.out.println("Pivot null la matrice est donc singulire");
                    break;
                }
            } catch (Exception e) {
            }

        }
        for (int i = 0; i < this.res.size(); i++) {
            System.out.println("Pour k = " + i);
            this.res.get(i).affichage();
        }
        System.out.println("Solution not ok");
//        System.out.print("[");
        n = A.getDim();
        Matrice solution = new Matrice(n, 1);
        Matrice resulta = this.res.get(res.size() - 1);
        try {
            double bn = resulta.getIJ(n - 1, n);
            double ann = resulta.getIJ(n - 1, n - 1);
            solution.setIJ(n - 1, 0, (bn / ann));
            for (int i = n - 2; i >= 0; i--) {
                double somme = 0;
                for (int j = i + 1; j < n; j++) {
                    somme = somme + resulta.getIJ(i, j) * solution.getIJ(j, 0);
                }
                double val = 1 / (resulta.getIJ(i, i)) * (resulta.getIJ(i, i + 1) - somme);
                solution.setIJ(i, 0, val);
            }
        } catch (OverflowException ex) {
            Logger.getLogger(Gauss.class.getName()).log(Level.SEVERE, null, ex);
        }
        solution.affichage();
    }

    /**
     * genere la matrice principale en fusionnant A et B
     */
    public void matGene() {
        if (this.A.isMatriceCarre()) {
            Matrice matrice = new Matrice(A.getDim(), A.getDim() + B.getX());
            for (int i = 0; i < A.getDim(); i++) {
                for (int j = 0; j < A.getDim(); j++) {
                    try {
                        matrice.setIJ(i, j, this.A.getIJ(i, j));
                    } catch (OverflowException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    matrice.setIJ(i, A.getDim(), this.B.getIJ(i, 0));
                } catch (OverflowException e) {
                    e.printStackTrace();
                }
            }
            this.generale = matrice;
        }
    }
}
