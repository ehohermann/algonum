/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.Lineaire.Gauss;

import algonum.matrice.Matrice;
import algonum.matrice.OverflowException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author hermann
 */
public class GaussPatial extends Gauss {

    public GaussPatial(Matrice A, Matrice B) {
        super(A, B);
    }

    public void runWithPartialPivot() {
        System.out.println("Methode de Gauss sans pivot");
        this.matGene();
        System.out.println("Matrice A");
        this.A.affichage();
        System.out.println("Matrice B");
        this.B.affichage();
        System.out.println("Matrice Augmenté");
        this.generale.affichage();
        int n = this.A.getX();
        this.res.add(this.generale);
        System.out.println(n);
        /**
         * nombre d'itération de l'algo
         */
        for (int k = 1; k <= n - 1; k++) {
            Matrice resNew = new Matrice(this.generale.getY(), this.generale.getX());

            /**
             * ***********************************************************************
             *
             * PIVOT DE LA MATRICE *
             *
             * ************************************************************************
             */
            /**
             * determination du pivot max
             */
            List l = new LinkedList();
            double max = 0;
            int indexe = k;
            for (int i = k; i <= n; i++) {
                try {
                    double val = Math.abs(res.get(k - 1).getIJ(i - 1, k - 1));
                    if (val > max) {
                        max = val;
                        indexe = i;
                    }
                } catch (Exception e) {
                }
            }
            Matrice resPivo = new Matrice(this.generale.getY(), this.generale.getX());
            /**
             * remplisage de la matrice avec changement de pivot
             */
            for (int a = 1; a <= n + 1; a++) {
                for (int b = 1; b <= n + 1; b++) {
                    try {
                        resPivo.setIJ(a - 1, b - 1, res.get(k - 1).getIJ(a - 1, b - 1));
                    } catch (Exception e) {
                    }
                }
            }

            /**
             * remplacement des lignes au niveau de la matrice augmenté
             */
            for (int i = k; i <= n; i++) {
                try {
                    double temp = resPivo.getIJ(k - 1, i - 1);
                    resPivo.setIJ(k - 1, i - 1, resPivo.getIJ(indexe - 1, i - 1));
                    resPivo.setIJ(indexe - 1, i - 1, temp);
                } catch (OverflowException e) {
                }
            }
            System.out.println("nombre de reslutats " + this.res.size());
            System.out.println("tours " + k);
            /**
             * ****************************************************************
             *
             * FIN DU PIVOT
             *
             * ****************************************************************
             */
            /**
             * ****************************************************************
             *
             * CALCUL DE LA NOUVEL MATRICE
             *
             * ****************************************************************
             */
            /**
             * parcourt des lignes
             */
            for (int i = 1; i <= k; i++) {
                for (int j = 1; j <= n + 1; j++) {
                    try {
                        resNew.setIJ(i - 1, j - 1, resPivo.getIJ((i - 1), (j - 1)));
                    } catch (OverflowException e) {
                    }
                }
            }
            for (int i = k + 1; i <= n; i++) {
                for (int j = 1; j <= k; j++) {
                    try {
                        resNew.setIJ(i - 1, j - 1, 0.0);
                    } catch (OverflowException e) {
                    }
                }
            }
            for (int i = k + 1; i <= n; i++) {
                for (int j = k + 1; j <= n + 1; j++) {
                    try {
                        double Aijk = resPivo.getIJ(i - 1, j - 1);
                        double Aikk = resPivo.getIJ(i - 1, k - 1);
                        double Akkk = resPivo.getIJ(k - 1, k - 1);
                        double Akjk = resPivo.getIJ(k - 1, j - 1);
                        double val = Aijk - (Aikk / Akkk * Akjk);
                        resNew.setIJ(i - 1, j - 1, val);
                    } catch (OverflowException e) {
                    }
                }
            }

            this.res.add(resNew);
        }
        for (int i = 0; i < this.res.size(); i++) {
            System.out.println("Pour k = " + i);
            this.res.get(i).affichage();
        }
        System.out.println("Solution");
        System.out.print("[");
        for (int i = 0; i < this.A.getDim(); i++) {
            try {
                System.out.println(this.res.get(this.A.getDim() - 1).getIJ(i, this.A.getDim()));
            } catch (OverflowException e) {
            }
        }
        System.out.print("]");
    }

}
