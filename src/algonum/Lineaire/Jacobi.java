/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algonum.Lineaire;

import algonum.matrice.Matrice;
import algonum.matrice.OverflowException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hermann
 */
public class Jacobi {

    Matrice A;
    Matrice B;
    Matrice generale;
    /**
     * les resultats de la matrice
     */
    ArrayList<Matrice> res = new ArrayList<Matrice>();

    public Jacobi(Matrice A, Matrice B) {
        if (A.isMatriceCarre()) {
            this.A = A;
        } else {
            System.out.println("la matrice n'est pas carré");
        }
        this.B = B;
    }

    public Matrice getA() {
        return A;
    }

    public void setA(Matrice A) {
        this.A = A;
    }

    public Matrice getB() {
        return B;
    }

    public void setB(Matrice B) {
        this.B = B;
    }

    public Matrice getGenerale() {
        return generale;
    }

    public void setGenerale(Matrice generale) {
        this.generale = generale;
    }

    public ArrayList<Matrice> getRes() {
        return res;
    }

    public void setRes(ArrayList<Matrice> res) {
        this.res = res;
    }

    /**
     * genere la matrice augmenté en fusionnant A et B
     */
    public void matGene() {
        if (this.A.isMatriceCarre()) {
            Matrice matrice = new Matrice(A.getDim(), A.getDim() + B.getX());
            for (int i = 0; i < A.getDim(); i++) {
                for (int j = 0; j < A.getDim(); j++) {
                    try {
                        matrice.setIJ(i, j, this.A.getIJ(i, j));
                    } catch (OverflowException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    matrice.setIJ(i, A.getDim(), this.B.getIJ(i, 0));
                } catch (OverflowException e) {
                    e.printStackTrace();
                }
            }
            this.generale = matrice;
        }
    }

    public void run(Matrice init) {
        System.out.println("Methode de jacobi");
        this.matGene();
        System.out.println("Matrice A");
        this.A.affichage();
        System.out.println("Matrice B");
        this.B.affichage();
        System.out.println("Matrice Augmenté");
        this.generale.affichage();

        System.out.println("Donné initiale");
        Matrice sol = new Matrice(this.B.getY(), this.B.getX());

        sol.affichage();
        init.affichage();
        /**
         * remplie la nouvel valeur de des soltions
         */
        for (int a = 0; a < 100000; a++) {
            for (int i = 0; i < this.A.getDim(); i++) {
                double val = 0;
                double somme = 0;
                try {
                    for (int j = 0; j < A.getDim(); j++) {
                        somme = somme + A.getIJ(i, j) * init.getIJ(j, 0);
                    }
                    val = (1 / A.getIJ(i, i)) * (this.B.getIJ(i, 0) - somme);
                    sol.setIJ(i, 0, val);
                } catch (OverflowException e) {
                    Logger.getLogger(GaussJordan.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            sol.affichage();
            init = sol;
        }
    }
}
